package com.spring.springdatarest.repository;

import com.spring.springdatarest.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

/**This repository is required to access the database data of the subject entity*/
public interface SubjectRepository extends JpaRepository<Subject,Integer> {
}
