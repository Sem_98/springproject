package com.spring.springdatarest.repository;

import com.spring.springdatarest.model.Mark;
import org.springframework.data.jpa.repository.JpaRepository;

/**This repository is required to access the database data of the mark entity*/
public interface MarkRepository extends JpaRepository<Mark,Integer> {
}
