package com.spring.springdatarest.repository;

import com.spring.springdatarest.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**This repository is required to access the database data of the student entity*/
public interface StudentRepository extends JpaRepository<Student, Integer>{
}
