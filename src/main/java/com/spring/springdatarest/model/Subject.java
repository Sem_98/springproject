package com.spring.springdatarest.model;

import javax.persistence.*;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

/**This is subject entity*/
@Entity
@Table(name = "subject")
@Getter
@Setter
@NoArgsConstructor
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id; //Identifier field
    private String name; //Name of subject field
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mark_id")
    private Mark mark; //Mark field
    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student; //Student field
}
