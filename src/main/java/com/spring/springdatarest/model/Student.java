package com.spring.springdatarest.model;

import javax.persistence.*;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import java.util.List;

/**This is student entity*/
@Entity
@Table(name = "student")
@Getter
@Setter
@NoArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id; //Identifier field
    private String name; //Name of student field
    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    private List<Subject> subjectsList; //List of subjects field
}

