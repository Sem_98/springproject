package com.spring.springdatarest.model;

import javax.persistence.*;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

/**This is mark entity*/
@Entity
@Table(name = "mark")
@Getter
@Setter
@NoArgsConstructor
public class Mark {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id; //Identifier field
    private int mark; //Mark field
    @OneToOne(mappedBy = "mark")
    private Subject subject; //Subject field
}

