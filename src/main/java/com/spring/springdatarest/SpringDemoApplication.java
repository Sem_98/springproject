package com.spring.springdatarest;

import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**This class starts the program
 * @author Vlad Semeniuk
 * @version 1.8
 */
@SpringBootApplication
@NoArgsConstructor
public class SpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemoApplication.class, args);
	}
}
